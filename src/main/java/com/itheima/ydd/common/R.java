package com.itheima.ydd.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description
 */

@Data
public class R {

    private Integer errno; //编码：1成功，0和其它数字为失败

    private String errmsg; //错误信息

    private Object data; //数据

    //泛型: 定义时类型不明确, 调用时类型明确
    public static  R success(Object object) {
        R r = new R();
        r.data = object;
        r.errno = 0;
        return r;
    }

    public static  R error(String msg) {
        R r = new R();
        r.errmsg = msg;
        r.errno = 1;
        return r;
    }

}
