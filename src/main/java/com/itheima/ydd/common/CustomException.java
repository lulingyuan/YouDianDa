package com.itheima.ydd.common;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description
 */
public class CustomException extends RuntimeException{
    public CustomException() {
    }

    public CustomException(String message) {
        super(message);
    }
}
