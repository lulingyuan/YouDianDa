package com.itheima.ydd.common;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description 全局异常处理器
 */
@RestControllerAdvice
public class ProjectExceptionHandler {

    //自定义业务异常统一处理
    @ExceptionHandler(CustomException.class)
    public R CustomExceptionHandler(CustomException e){
        return R.error(e.getMessage());
    }

    //系统其他异常统一处理
    @ExceptionHandler(Exception.class)
    public R exceptionHandler(Exception e){
        e.printStackTrace();
        return R.error("未知错误!");
    }
}
