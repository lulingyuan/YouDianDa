package com.itheima.ydd.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.ydd.dao.SysConfigDao;
import com.itheima.ydd.entity.SysConfig;
import org.springframework.stereotype.Service;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description
 */
@Service
public class SysConfigService extends ServiceImpl<SysConfigDao, SysConfig> {
}
