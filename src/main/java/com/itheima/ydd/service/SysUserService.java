package com.itheima.ydd.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.ydd.dao.SysUserDao;
import com.itheima.ydd.entity.SysUser;
import org.springframework.stereotype.Service;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description
 */
@Service
public class SysUserService extends ServiceImpl<SysUserDao, SysUser> {
}
