package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 标签类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ydd_tag")
public class Tag {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String tagname;
    private Integer default_data;
}
