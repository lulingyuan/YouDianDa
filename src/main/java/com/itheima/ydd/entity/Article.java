package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 文章对象
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ydd_article")
public class Article {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String title;
    private String author;
    private String keywords;
    private String description;
    private String content;
    private String pic;
    private Long click;
    private Integer ishot;
    private Integer istop;
    private Long create_date;
    private Long update_date;
    private Integer cateid;
    private String tags;
    private Integer status;
    private String lovenum;
    private Integer user_id;
    private String default_data;
}
