package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 内容分类
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ydd_cate")
public class Cate {
    @TableId(type = IdType.AUTO)
    private Integer id;//分类id
    private String catename;//分类名
    private String icon;//字段图标
    private Integer sort_num;//排序数字
    private Integer default_data;//是否为默认数据,null表示否,1表示是
}
