package com.itheima.ydd.entity;

import lombok.Data;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 文件返回
 */
@Data
public class FileResult {
    //文件的云路径
    private String savePath;
    //文件名
    private String name;
    //文件类型
    private String mime;
    //文件大小(字节)
    private Integer size;
}
