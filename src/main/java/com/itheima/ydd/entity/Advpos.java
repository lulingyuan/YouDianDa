package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 广告
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("ydd_advpos")
public class Advpos {
    private Integer id;
    private String advposname;
    private String advposdesc;
    private String advpossize;
    private String default_data;
}
