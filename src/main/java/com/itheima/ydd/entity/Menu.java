package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 菜单
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
public class Menu implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 父级编号
     */
    private Integer pid;

    /**
     * 路由名称
     */
    private String path;

    /**
     * 组件路径
     */
    private String component;

    /**
     * 菜单名称
     */
    private String title;

    /**
     * 图标
     */
    private String icon;

    /**
     * 是否在菜单中显示，0不显示，1显示
     */
    private Integer is_show;

    /**
     * 是否缓存，0否，1是
     */
    private Integer is_cache;

    /**
     * 是否外联，0否，1是
     */
    private Integer is_link;

    /**
     * 重定向地址
     */
    private String redirect;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime create_date;

    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime update_date;

    /**
     * 删除标记
     */
    private Integer del_flag;

    /**
     * 类型，1是菜单，2按钮
     */
    private Integer type;

    /**
     * 排序
     */
    private BigDecimal sort;

    /**
     * 权限标识
     */
    private String mark;

    /**
     * 是否为默认数据，null表示不是，1表示是
     */
    private String default_data;

    @TableField(exist = false)
    List<Menu> children;

}
