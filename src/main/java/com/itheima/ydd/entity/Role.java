package com.itheima.ydd.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description 角色表
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_role")
public class Role {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 创建时间
     */
    private LocalDateTime create_date;

    /**
     * 更新时间
     */
    private LocalDateTime update_date;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 删除标记
     */
    private String del_flag;

    /**
     * 是否为超级管理员
     */
    private Integer superadmin;

    /**
     * 是否为默认数据，null表示不是，1表示是
     */
    private String default_data;

}
