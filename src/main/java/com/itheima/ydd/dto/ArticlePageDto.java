package com.itheima.ydd.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

/**
 * @author yuan
 * @date 2022/11/13
 * @Description
 */
@Data
public class ArticlePageDto {

    private static final long serialVersionUID = 1L;

    @JsonProperty("count")
    private long total;

    @JsonProperty("totalPages")
    private long pages;

    @JsonProperty("pageSize")
    private long size;

    @JsonProperty("currentPage")
    private long current;

    @JsonProperty("data")
    private List<Object> records;

}
