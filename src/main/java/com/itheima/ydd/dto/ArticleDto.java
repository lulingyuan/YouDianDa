package com.itheima.ydd.dto;

import lombok.Data;

/**
 * @autehor yuan
 * @date 2022/11/13
 * @Description
 */
@Data
public class ArticleDto {
    private String catename;
    private String click;
    private String id;
    private String title;
    private String pic;
    private String lovenum;

}
