package com.itheima.ydd;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description
 */
@SpringBootApplication
@MapperScan("com.itheima.ydd.dao")
public class YddApplication {
    public static void main(String[] args) {
        SpringApplication.run(YddApplication.class,args);
    }
}
