package com.itheima.ydd.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.ydd.common.R;
import com.itheima.ydd.entity.SysConfig;
import com.itheima.ydd.service.SysConfigService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description 基础模块,后台
 */
@Slf4j
@RestController
@RequestMapping("/index")
public class SysConfigController {

    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 获取系统配置
     * @return R
     */
    @GetMapping("/getConfig")
    public R getConfig(){
        //查询管理系统配置
        LambdaQueryWrapper<SysConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysConfig::getTab_value,"system");
        List<SysConfig> list = sysConfigService.list(wrapper);

        //组装数据返回
        Map<String,String> resMap = new HashMap<>();
        list.forEach(sysConfig -> resMap.put(sysConfig.getAttr_key(),sysConfig.getAttr_value()));
        return R.success(resMap);
    }
}
