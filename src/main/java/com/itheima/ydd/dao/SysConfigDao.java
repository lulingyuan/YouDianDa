package com.itheima.ydd.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.ydd.entity.SysConfig;

/**
 * @autehor yuan
 * @date 2022/11/12
 * @Description
 */
public interface SysConfigDao extends BaseMapper<SysConfig> {
}
