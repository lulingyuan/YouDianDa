###  **优点达** 



#### 介绍
优点达

#### 软件架构
软件架构说明


 安装教程

1. 略

使用说明

1.  使用Git过程中,必须通过创建分支进行开发,坚决禁止在主分支上直接开发
2.  禁止提交有bug的代码,有问题组内相互讨论或者获取同学或他人的协助
3.  push前务必先pull
4.  不允许修改项目包名类名等,如果有骨架有bug可以组内开会讨论后进行修改

参与贡献

1.  卢凌元
2.  贺阳春
3.  王佳
4.  放鹏程
5.  高学东
6.  鲁子晨
7.  王林海


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
